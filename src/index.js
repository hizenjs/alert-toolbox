import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';

ReactDOM.render(
	<React.StrictMode>
		<App />
		<Alert stack={{ limit: 3 }} />
	</React.StrictMode>,
	document.getElementById('root')
);

serviceWorker.unregister();
