import { StyleSheet } from 'aphrodite';
import COLORS from '../styles/Colors';

const loader = StyleSheet.create({
	wrapper : {
		position        : 'absolute',
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		top             : 0,
		left            : 0,
		right           : 0,
		bottom          : 0,
		width           : '100%',
		height          : '100%',
		backgroundColor : COLORS.BACKGROUND,
		color           : COLORS.PRIMARY
	},
	button  : {
		padding         : '10px 20px',
		border          : 'none',
		borderRadius    : '4px',
		backgroundColor : COLORS.PANNEL,
		color           : COLORS.DEFAULT,
		fontSize        : '14px',
		cursor          : 'pointer',
		transition      : '.3s background',
		':hover'        : {
			color : COLORS.PRIMARY
		}
	}
});

const login = StyleSheet.create({
	wrapper   : {
		position        : 'absolute',
		display         : 'flex',
		flexDirection   : 'column',
		justifyContent  : 'center',
		alignItems      : 'center',
		width           : '100%',
		height          : '100%',
		backgroundColor : COLORS.BACKGROUND
	},
	title     : {
		color         : COLORS.CONTRAST,
		fontWeight    : 400,
		textTransform : 'uppercase',
		fontSize      : '22px',
		marginBottom  : '1.5em',
		letterSpacing : '10px'
	},
	subtitle  : {
		color        : COLORS.CONTRAST,
		fontWeight   : 400,
		fontSize     : '18px',
		marginTop    : 0,
		marginBottom : '1em',
		width        : '100%'
	},
	container : {
		borderRadius    : '8px',
		padding         : '1em 2em',
		borderColor     : 'red',
		borderWidth     : '1px',
		display         : 'flex',
		flexDirection   : 'column',
		justifyContent  : 'center',
		alignItems      : 'center',
		width           : '30%',
		backgroundColor : COLORS.CARD,
		color           : COLORS.PRIMARY,
		boxShadow       : '0px 46px 75px -35px rgba(0,0,0,0.8)'
	},
	input     : {
		cursor          : 'pointer',
		margin          : '8px 15px',
		borderStyle     : 'solid',
		borderRadius    : '5px',
		borderWidth     : '1px',
		lineHeight      : '11px',
		height          : '35px',
		width           : '94%',
		padding         : '0 10px',
		fontSize        : '11px',
		fontWeight      : 'bold',
		borderColor     : COLORS.PLACEHOLDER,
		backgroundColor : COLORS.CARD,
		color           : COLORS.PLACEHOLDER,
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		transition      : '.3s',
		':focus'        : {
			outline     : 'none',
			borderColor : COLORS.CONTRAST
		},
		'::placeholder' : {
			textTransform : 'uppercase',
			color         : COLORS.PLACEHOLDER
		}
	},
	button    : {
		cursor          : 'pointer',
		margin          : '15px 15px',
		borderStyle     : 'solid',
		borderRadius    : '5px',
		border          : 'none',
		lineHeight      : '11px',
		height          : '35px',
		width           : '100%',
		padding         : '0 10px',
		fontSize        : '11px',
		fontWeight      : 'bold',
		backgroundColor : COLORS.PRIMARY,
		color           : COLORS.CARD,
		marginBottom    : '15px',
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		transition      : '.3s',
		':hover'        : {
			color : COLORS.CONTRAST
		},
		':focus'        : {
			color   : COLORS.CONTRAST,
			outline : 'none'
		}
	},
	methode   : {
		marginTop  : '15px',
		fontSize   : '12px',
		cursor     : 'pointer',
		color      : COLORS.PLACEHOLDER,
		transition : '.3s',
		':hover'   : {
			color : COLORS.CONTRAST
		}
	},
	question  : {
		marginRight : '4px',
		fontSize    : '12px',
		cursor      : 'pointer',
		color       : COLORS.PLACEHOLDER,
		transition  : '.3s',
		':hover'    : {
			color : COLORS.PLACEHOLDER
		}
	}
});

const header = StyleSheet.create({
	wrapper         : {
		display         : 'flex',
		flexDirection   : 'row',
		justifyContent  : 'space-between',
		alignItems      : 'center',
		width           : '100%',
		height          : '60px',
		backgroundColor : COLORS.CARD
	},
	brand           : {
		display        : 'flex',
		justifyContent : 'flex-start',
		alignItems     : 'center',
		height         : '60px',
		width          : '20%'
	},
	brandImage      : {
		objectFit  : 'contain',
		width      : '50px',
		height     : '50px',
		marginLeft : '26px'
	},
	brandName       : {
		color         : COLORS.CONTRAST,
		fontSize      : '16px',
		textTransform : 'uppercase',
		letterSpacing : '4px',
		marginLeft    : '26px'
	},
	centered        : {
		height         : '60px',
		width          : '55%',
		display        : 'flex',
		justifyContent : 'space-around',
		alignItems     : 'center'
	},
	format          : {
		display        : 'flex',
		flexDirection  : 'row',
		justifyContent : 'center',
		alignItems     : 'center'
	},
	formatLabel     : {
		fontSize      : '10px',
		fontWeight    : 'bold',
		textTransform : 'uppercase',
		color         : COLORS.PLACEHOLDER
	},
	inputFile       : {
		cursor          : 'pointer',
		margin          : '15px 15px',
		borderStyle     : 'solid',
		borderRadius    : '5px',
		borderWidth     : '1px',
		lineHeight      : '11px',
		height          : '25px',
		padding         : '0 10px',
		textTransform   : 'uppercase',
		fontSize        : '11px',
		fontWeight      : 'bold',
		borderColor     : COLORS.PLACEHOLDER,
		backgroundColor : COLORS.CARD,
		color           : COLORS.PLACEHOLDER,
		marginBottom    : '15px',
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		transition      : '.3s',
		':focus'        : {
			outline     : 'none',
			borderColor : COLORS.CONTRAST
		},
		'::placeholder' : {
			color : COLORS.PLACEHOLDER
		}
	},
	inputFileIcon   : {
		fontSize    : '16px',
		marginRight : '10px'
	},
	account         : {
		width          : '23%',
		height         : '60px',
		display        : 'flex',
		justifyContent : 'flex-end',
		alignItems     : 'center',
		paddingRight   : '2%'
	},
	button          : {
		borderRadius    : '5px',
		border          : 'none',
		padding         : '0 1em',
		textTransform   : 'uppercase',
		fontWeight      : '700',
		backgroundColor : COLORS.PRIMARY,
		color           : COLORS.CARD,
		height          : '28px',
		display         : 'flex',
		justifyContent  : 'flex-end',
		alignItems      : 'center',
		marginRight     : '30px',
		cursor          : 'pointer',
		':focus'        : { outline: 'none' }
	},

	accountDropdown : {
		display    : 'flex',
		alignItems : 'center',
		cursor     : 'pointer'
	},
	accountImage    : {
		borderRadius : '8px',
		width        : '40px',
		height       : '40px',
		marginRight  : '5px',
		objectFit    : 'contain',
		background   : COLORS.PANNEL
	},
	iconDropdown    : {
		width      : '30px',
		fontSize   : '16px',
		lineHeight : '30px'
	}
});

const sidenav = StyleSheet.create({
	wrapper : {
		display         : 'flex',
		flexDirection   : 'column',
		justifyContent  : 'flex-start',
		alignItems      : 'center',
		position        : 'absolute',
		width           : '8%',
		left            : 0,
		bottom          : 0,
		top             : '60px',
		backgroundColor : COLORS.PANNEL
	},
	item    : {
		display        : 'flex',
		flexDirection  : 'column',
		justifyContent : 'center',
		alignItems     : 'center',
		height         : '70px',
		backgroung     : 'red',
		textTransform  : 'uppercase',
		fontSize       : '9px',
		lineHeight     : '2em',
		width          : '100%',
		fontWeight     : 'bold',
		transition     : '200ms',
		cursor         : 'pointer',
		color          : COLORS.PLACEHOLDER,
		':hover'       : {
			color : COLORS.CONTRAST
		}
	},
	active  : {
		background : COLORS.PLACEHOLDER,
		color      : COLORS.CONTRAST
	}
});

const pannel = StyleSheet.create({
	wrapper           : {
		display         : 'flex',
		flexDirection   : 'column',
		justifyContent  : 'flex-start',
		alignItems      : 'flex-start',
		position        : 'absolute',
		width           : '25%',
		right           : 0,
		bottom          : 0,
		top             : '60px',
		overflow        : 'auto',
		backgroundColor : COLORS.PANNEL
	},
	pannelName        : {
		display        : 'flex',
		justifyContent : 'flex-start',
		alignItems     : 'center',
		height         : '50px',
		width          : '90%',
		padding        : '0 5%',
		borderBottom   : '1px solid rgba(108, 108, 123, 0.3)',
		color          : COLORS.CONTRAST
	},
	pannelIcon        : {
		marginRight : '5%',
		fontSize    : '20px',
		color       : COLORS.PLACEHOLDER
	},
	container         : {
		cursor          : 'default',
		backgroundColor : COLORS.CARD,
		color           : COLORS.PLACEHOLDER,
		borderRadius    : '8px',
		height          : '60px',
		width           : '80%',
		padding         : '5%',
		margin          : '5%'
	},
	section           : {
		display        : 'flex',
		flexDirection  : 'column',
		justifyContent : 'flex-start',
		alignItems     : 'flex-start',
		width          : '90%',
		padding        : '0 5%',
		borderBottom   : '1px solid rgba(108, 108, 123, 0.3)'
	},
	sectionRow        : {
		display        : 'flex',
		flexDirection  : 'row',
		justifyContent : 'space-between',
		alignItems     : 'center',
		paddingBottom  : '15px',
		paddingTop     : '15px'
	},
	floatNotif        : {
		position  : 'fixed',
		marginTop : '1%',
		right     : '26%'
	},
	floatButton       : {
		borderRadius    : '5px',
		border          : 'none',
		padding         : '0 1em',
		textTransform   : 'uppercase',
		fontWeight      : '700',
		backgroundColor : COLORS.PRIMARY,
		color           : COLORS.CARD,
		height          : '28px',
		display         : 'flex',
		justifyContent  : 'flex-end',
		alignItems      : 'center',
		cursor          : 'pointer',
		':focus'        : { outline: 'none' }
	},
	elementsRow       : {
		display        : 'flex',
		flexDirection  : 'row',
		justifyContent : 'space-between',
		alignItems     : 'center',
		paddingBottom  : '0px',
		paddingTop     : '0px',
		width          : '100%'
	},
	titleSection      : {
		textTransform : 'uppercase',
		fontWeight    : 'bold',
		fontSize      : '10px',
		color         : COLORS.PLACEHOLDER,
		marginBottom  : 0
	},
	alignList         : {
		listStyle      : 'none',
		display        : 'flex',
		justifyContent : 'flex-start',
		alignItems     : 'center',
		padding        : 0
	},
	alignItemLeft     : {
		borderTopLeftRadius    : '4px',
		borderBottomLeftRadius : '4px'
	},
	alignItemRight    : {
		borderTopRightRadius    : '4px',
		borderBottomRightRadius : '4px'
	},
	alignItem         : {
		display        : 'flex',
		justifyContent : 'center',
		alignItems     : 'center',
		textAlign      : 'center',
		color          : COLORS.PLACEHOLDER,
		background     : COLORS.CARD,
		width          : '40px',
		fontSize       : '20px',
		height         : '30px',
		transition     : '200ms',
		':hover'       : {
			color : COLORS.CONTRAST
		}
	},
	alignItemText     : {
		display        : 'flex',
		justifyContent : 'center',
		alignItems     : 'center',
		textAlign      : 'center',
		color          : COLORS.PLACEHOLDER,
		background     : COLORS.CARD,
		width          : '25%',
		textTransform  : 'uppercase',
		fontWeight     : 'bold',
		fontSize       : '10px',
		height         : '30px',
		transition     : '200ms',
		cursor         : 'default',
		':hover'       : {
			color : COLORS.CONTRAST
		}
	},
	alignActive       : {
		color      : COLORS.CONTRAST,
		background : COLORS.PLACEHOLDER
	},
	inputSelect       : {
		cursor          : 'pointer',
		margin          : '15px 0',
		borderStyle     : 'solid',
		borderRadius    : '5px',
		borderWidth     : '1px',
		lineHeight      : '11px',
		height          : '25px',
		width           : '100%',
		padding         : '0 10px',
		textTransform   : 'uppercase',
		fontSize        : '11px',
		fontWeight      : 'bold',
		borderColor     : COLORS.PLACEHOLDER,
		backgroundColor : COLORS.CARD,
		color           : COLORS.PLACEHOLDER,
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		transition      : '.3s',
		':focus'        : {
			outline     : 'none',
			borderColor : COLORS.CONTRAST
		},
		'::placeholder' : {
			color : COLORS.PLACEHOLDER
		}
	},
	fontSizer         : {
		display        : 'flex',
		alignItems     : 'center',
		justifyContent : 'space-between',
		width          : '100%'
	},
	fontSizeIcon      : {
		color : COLORS.PLACEHOLDER
	},
	inputSimpleValue  : {
		textAlign       : 'center',
		cursor          : 'pointer',
		margin          : '15px 0',
		borderStyle     : 'solid',
		borderRadius    : '5px',
		borderWidth     : '1px',
		lineHeight      : '11px',
		height          : '25px',
		width           : '35px',
		padding         : '0 10px',
		textTransform   : 'uppercase',
		fontSize        : '11px',
		fontWeight      : 'bold',
		borderColor     : COLORS.PLACEHOLDER,
		backgroundColor : COLORS.CARD,
		color           : COLORS.PLACEHOLDER,
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		transition      : '.3s',
		':focus'        : {
			outline     : 'none',
			borderColor : COLORS.CONTRAST
		},
		'::placeholder' : {
			color : COLORS.PLACEHOLDER
		}
	},
	colorSelector     : {
		display        : 'flex',
		justifyContent : 'flex-start',
		alignItems     : 'center'
	},
	inputHex          : {
		textAlign       : 'center',
		cursor          : 'pointer',
		margin          : '15px 0',
		borderStyle     : 'solid',
		borderRadius    : '5px',
		borderWidth     : '1px',
		lineHeight      : '11px',
		height          : '25px',
		width           : '50px',
		padding         : '0 10px',
		textTransform   : 'uppercase',
		fontSize        : '11px',
		fontWeight      : 'bold',
		borderColor     : COLORS.PLACEHOLDER,
		backgroundColor : COLORS.CARD,
		color           : COLORS.PLACEHOLDER,
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		transition      : '.3s',
		':focus'        : {
			outline     : 'none',
			borderColor : COLORS.CONTRAST
		},
		'::placeholder' : {
			color : COLORS.PLACEHOLDER
		}
	},
	colorOutline      : {
		margin         : '15px 0',
		marginRight    : '15px',
		width          : '20px',
		height         : '20px',
		borderRadius   : '25px',
		border         : '1px solid white',
		display        : 'flex',
		justifyContent : 'center',
		alignItems     : 'center',
		borderColor    : COLORS.PLACEHOLDER
	},
	colorDot          : {
		width        : '12px',
		height       : '12px',
		borderRadius : '25px'
	},
	durationIcon      : {
		marginRight : '15px',
		color       : COLORS.PLACEHOLDER
	},
	durationSelector  : {
		display        : 'flex',
		flexDirection  : 'row',
		alignItems     : 'center',
		justifyContent : 'flex-start',
		width          : '100%'
	},
	centeredInputText : {
		height     : '25px',
		marginLeft : '8px'
	},
	inputNumValue     : {
		textAlign       : 'right',
		cursor          : 'pointer',
		margin          : '15px 0',
		borderStyle     : 'solid',
		borderRadius    : '5px',
		borderWidth     : '1px',
		lineHeight      : '11px',
		height          : '25px',
		width           : '50px',
		padding         : '0 2px',
		textTransform   : 'uppercase',
		fontSize        : '11px',
		fontWeight      : 'bold',
		borderColor     : COLORS.PLACEHOLDER,
		backgroundColor : COLORS.CARD,
		color           : COLORS.PLACEHOLDER,
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		transition      : '.3s',
		':focus'        : {
			outline     : 'none',
			borderColor : COLORS.CONTRAST
		},
		'::placeholder' : {
			color : COLORS.PLACEHOLDER
		}
	},
	titlesRow         : {
		width          : '100%',
		display        : 'flex',
		flexDirection  : 'row',
		justifyContent : 'space-around',
		alignItems     : 'center'
	},
	exampleBox        : {
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		margin          : '40px 0',
		width           : '70px',
		height          : '30px',
		borderRadius    : '6px',
		backgroundColor : COLORS.CARD,
		fontWeight      : 'bold',
		color           : COLORS.PANNEL,
		fontSize        : '10px'
	},
	modalGrid         : {
		display  : 'flex',
		width    : '100%',
		flexWrap : 'wrap'
	},
	modalTitle        : {
		width         : '100%',
		color         : COLORS.CONTRAST,
		fontSize      : '10px',
		fontWeight    : 'bold',
		textTransform : 'uppercase'
	},
	modalGridItem     : {
		display        : 'flex',
		justifyContent : 'flex-start',
		alignItems     : 'center',
		lineHeight     : '3em',
		width          : '31%',
		padding        : '1%',
		color          : COLORS.PLACEHOLDER,
		fontSize       : '10px',
		fontWeight     : 'bold',
		textTransform  : 'uppercase',
		whiteSpace     : 'nowrap',
		overflow       : 'hidden',
		textOverflow   : 'ellipsis'
	},
	modalCheckBoxLeft : {
		marginRight : '10px'
	},
	modalError        : {
		color          : COLORS.INFO,
		fontSize       : '10px',
		fontWeight     : 'bold',
		textTransform  : 'uppercase',
		textAlign      : 'center',
		display        : 'flex',
		alignItems     : 'center',
		justifyContent : 'center',
		width          : '100%',
		height         : '300px',
		letterSpacing  : '2px',
		lineHeight     : '2em'
	},
	inputFile         : {
		cursor          : 'pointer',
		margin          : '15px 15px',
		borderStyle     : 'solid',
		borderRadius    : '5px',
		borderWidth     : '1px',
		lineHeight      : '11px',
		height          : '25px',
		padding         : '0 10px',
		textTransform   : 'uppercase',
		fontSize        : '11px',
		fontWeight      : 'bold',
		borderColor     : COLORS.PLACEHOLDER,
		backgroundColor : COLORS.CARD,
		color           : COLORS.PLACEHOLDER,
		marginBottom    : '15px',
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		transition      : '.3s',
		':focus'        : {
			outline     : 'none',
			borderColor : COLORS.CONTRAST
		},
		'::placeholder' : {
			color : COLORS.PLACEHOLDER
		}
	},
	inputFileIcon     : {
		fontSize    : '16px',
		marginRight : '10px'
	},
	inputConfig       : {
		marginLeft : 0,
		marginTop  : 0
	},
	rawInput          : {
		margin       : 0,
		marginBottom : '15px',
		width        : '96%',
		padding      : '2%',
		height       : '260px',
		background   : COLORS.CARD,
		borderRadius : '5px',
		overflow     : 'auto',
		fontSize     : '12px',
		lineHeight   : '1.3em',
		fontFamily   : 'Courier',
		color        : COLORS.PLACEHOLDER,
		border       : '1px solid white',
		borderColor  : COLORS.PLACEHOLDER
	}
});

const dropdown = StyleSheet.create({
	menu : {
		display        : 'flex',
		justifyContent : 'center',
		alignItems     : 'flex-start',
		flexDirection  : 'column',
		position       : 'absolute',
		zIndex         : 7,
		top            : '65px',
		right          : '10px',
		width          : '200px',
		background     : COLORS.CARD,
		borderRadius   : '8px',
		boxShadow      : '0px 6px 17px -4px rgba(0,0,0,0.46)'
	},
	item : {
		color      : COLORS.PLACEHOLDER,
		display    : 'flex',
		alignItems : 'center',
		transition : '200ms',
		':hover'   : {
			color : COLORS.CONTRAST
		}
	},
	icon : {
		marginLeft  : '14px',
		marginRight : '10px'
	}
});

const preview = StyleSheet.create({
	wrapper               : {
		display        : 'flex',
		flexDirection  : 'column',
		justifyContent : 'space-evenly',
		alignItems     : 'center',
		position       : 'absolute',
		width          : '67%',
		left           : '8%',
		bottom         : '0',
		top            : '60px',
		overflow       : 'hidden'
	},
	container             : {
		backgroundColor : COLORS.BACKGROUND,
		color           : COLORS.PLACEHOLDER,
		transition      : '300ms',
		borderRadius    : '8px',
		height          : '400px',
		width           : '60%',
		display         : 'flex',
		flexDirection   : 'column',
		justifyContent  : 'center',
		alignItems      : 'center'
	},
	content               : {
		transition : '300ms',
		width      : '60px',
		height     : '60px',
		objectFit  : 'contain',
		filter     : 'grayscale(100%)'
	},
	params                : {
		display        : 'flex',
		alignItems     : 'center',
		justifyContent : 'space-around'
	},
	iconButton            : {
		border         : '1px solid white',
		borderColor    : COLORS.PLACEHOLDER,
		borderRadius   : '4px',
		fontSize       : '11px',
		lineHeight     : '18px',
		width          : '18px',
		height         : '18px',
		display        : 'flex',
		alignItems     : 'center',
		justifyContent : 'center',
		color          : COLORS.PLACEHOLDER,
		cursor         : 'pointer'
	},
	scale                 : {
		color    : COLORS.PLACEHOLDER,
		fontSize : '10px',
		margin   : '0 10px',
		cursor   : 'default'
	},
	inputFile             : {
		cursor          : 'pointer',
		margin          : '15px 15px',
		borderStyle     : 'solid',
		borderRadius    : '5px',
		borderWidth     : '1px',
		lineHeight      : '11px',
		height          : '25px',
		padding         : '0 10px',
		textTransform   : 'uppercase',
		fontSize        : '11px',
		fontWeight      : 'bold',
		borderColor     : COLORS.PLACEHOLDER,
		backgroundColor : COLORS.CARD,
		color           : COLORS.PLACEHOLDER,
		marginBottom    : '15px',
		display         : 'flex',
		justifyContent  : 'center',
		alignItems      : 'center',
		transition      : '.3s',
		':focus'        : {
			outline     : 'none',
			borderColor : COLORS.CONTRAST
		},
		'::placeholder' : {
			color : COLORS.PLACEHOLDER
		}
	},
	inputFileIcon         : {
		fontSize    : '16px',
		marginRight : '10px'
	},
	rulerTop              : {
		opacity        : 0.4,
		position       : 'absolute',
		top            : '0px',
		width          : '100%',
		left           : '0%',
		height         : '16px',
		paddingLeft    : '16px',
		display        : 'flex',
		flexDirection  : 'row',
		justifyContent : 'flex-start',
		alignItems     : 'flex-end',
		borderBottom   : `1px solid ${COLORS.PLACEHOLDER}`
	},
	rulerLeft             : {
		opacity        : 0.4,
		position       : 'absolute',
		display        : 'flex',
		flexDirection  : 'column',
		justifyContent : 'flex-start',
		alignItems     : 'flex-end',
		top            : '0px',
		height         : '100%',
		left           : '0%',
		width          : '16px',
		paddingTop     : '16px',
		borderRight    : `1px solid ${COLORS.PLACEHOLDER}`
	},
	rulerInnerVertical    : {
		height          : '5px',
		width           : '4px',
		borderBottom    : `1px solid ${COLORS.PLACEHOLDER}`,
		':last-of-type' : {
			border : 'none'
		}
	},
	rulerGroupsVertical   : {
		display        : 'flex',
		flexDirection  : 'column',
		justifyContent : 'flex-start',
		alignItems     : 'flex-end',
		width          : '10px',
		height         : 'auto',
		borderBottom   : `1px solid ${COLORS.PLACEHOLDER}`
	},
	rulerInnerHorizontal  : {
		height          : '4px',
		width           : '5px',
		borderRight     : `1px solid ${COLORS.PLACEHOLDER}`,
		':last-of-type' : {
			border : 'none'
		}
	},
	rulerGroupsHorizontal : {
		display        : 'flex',
		flexDirection  : 'row',
		justifyContent : 'flex-end',
		alignItems     : 'flex-end',
		height         : '10px',
		borderRight    : `1px solid ${COLORS.PLACEHOLDER}`
	}
});

const anim = StyleSheet.create({
	config : {
		animationDuration       : '2s',
		animationIterationCount : 'infinite'
	}
});

export { loader, login, header, sidenav, pannel, dropdown, preview, anim };
