import React from 'react';
import { css } from 'aphrodite';
import { preview } from '../styles/Styles';
import { IoIosExpand } from 'react-icons/io';
import COLORS from '../styles/Colors';

export default class Preview extends React.Component {
	constructor (props) {
		super(props);
		this.state = {};
	}

	renderDotsVertical = () => {
		var indents = [];
		for (var i = 0; i < 10; i++) {
			indents.push(<div key={`i-v-${i}`} className={css(preview.rulerInnerVertical)} />);
		}
		var groups = [];
		for (let j = 0; j < 9; j++) {
			groups.push(
				<div key={`i-v-${j}`} className={css(preview.rulerGroupsVertical)}>
					{indents}
				</div>
			);
		}

		return (
			<React.Fragment>
				<div className={css(preview.rulerLeft)}>{groups}</div>
			</React.Fragment>
		);
	};
	renderDotsHorizontal = () => {
		var indents = [];
		for (var i = 0; i < 10; i++) {
			indents.push(<div key={`i-h-${i}`} className={css(preview.rulerInnerHorizontal)} />);
		}
		var groups = [];
		for (let j = 0; j < 12; j++) {
			groups.push(
				<div key={`g-h-${j}`} className={css(preview.rulerGroupsHorizontal)}>
					{indents}
				</div>
			);
		}

		return (
			<React.Fragment>
				<div className={css(preview.rulerTop)}>{groups}</div>
				{this.renderDotsVertical()}
			</React.Fragment>
		);
	};

	render () {
		let ratio = this.props.ratio;
		ratio = ratio.split(':');
		ratio = ratio[1] / ratio[0];
		let float = '100%';
		let height = null;
		if (ratio.toString().length >= 3) {
			ratio = ratio.toString().slice(2);
			float = [ ratio.slice(0, 2), ratio.slice(2) ];
			height = `${float[0]}.${float[1]}%`;
		}
		if (ratio.toString().length === 2) {
			height = `${ratio.toString()}%`;
		}
		if (ratio.toString().length === 1) {
			height = `50%`;
		}
		if (ratio.toString().length === 0) {
			height = `100%`;
		}

		const contentStyle = {
			width           : '90%',
			padding         : '0 5%',
			height          : height,
			background      : COLORS.CARD,
			display         : 'flex',
			flexDirection   : 'column',
			justifyContent  : 'center',
			alignItems      : 'center',
			backgroundColor : COLORS.PANNEL,
			color           : COLORS.PLACEHOLDER,
			borderRadius    : '8px'
		};

		return (
			<div className={css(preview.wrapper)}>
				{this.renderDotsHorizontal()}
				<div className={css(preview.container)}>
					<div style={contentStyle}>
						<iframe
							style={{ border: 'none' }}
							id='inlineFrameExample'
							title='Inline Frame Example'
							width='100%'
							height='100%'
							src='https://alert-toolbox.web.app'
						/>
						{/* <img className={css(preview.content)} src={logo} alt='preview' /> */}
					</div>
				</div>
				<div className={css(preview.params)}>
					<div className={css(preview.iconButton)}>-</div>
					<div className={css(preview.scale)}>100%</div>
					<div className={css(preview.iconButton)}>+</div>
					<button className={css(preview.inputFile)}>
						<IoIosExpand className={css(preview.inputFileIcon)} />Full Screen
					</button>
				</div>
			</div>
		);
	}
}
