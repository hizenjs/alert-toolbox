import React from 'react';
import { css } from 'aphrodite';
import { login } from '../styles/Styles';
import { auth, database } from '../services/Config';
import { Color } from 'custom-electron-titlebar';
import { titlebar } from './Header';
import Alert from 'react-s-alert';
import COLORS from '../styles/Colors';
import '../styles/Loader.css';

titlebar.updateBackground(Color.fromHex(COLORS.BACKGROUND));

class Auth extends React.Component {
	constructor (props) {
		super(props);
		this.state = {
			signin         : true,
			authProcessing : false
		};
		this.handleChangeEmail = this.handleChangeEmail.bind(this);
		this.handleChangePassword = this.handleChangePassword.bind(this);
	}

	handleChangeEmail (event) {
		this.setState({ email: event.target.value });
	}
	handleChangePassword (event) {
		this.setState({ password: event.target.value });
	}
	signMethode = () => {
		this.setState({ signin: !this.state.signin });
	};

	handleSubmit = () => {
		this.setState({ authProcessing: true });
		if (this.state.signin) {
			auth
				.signInWithEmailAndPassword(this.state.email, this.state.password)
				.then((res) => {
					console.log(res);
					this.setState({ authProcessing: false });
					this.props.handleAuth();
				})
				.catch((error) => {
					this.setState({ authProcessing: false });
					Alert.error(error.message, {
						position : 'top-right',
						effect   : 'slide',
						timeout  : 'none',
						offset   : 10
					});
				});
		}
		else {
			auth
				.createUserWithEmailAndPassword(this.state.email, this.state.password)
				.then((res) => {
					database.ref('users').child(res.user.uid).set({
						email : res.user.uid
					}, (error) => {
						if (error) {
							this.setState({ authProcessing: false });
							Alert.error(error, {
								position : 'top-right',
								effect   : 'slide',
								timeout  : 'none',
								offset   : 10
							});
						}
						else {
							this.setState({ authProcessing: false });
							this.props.handleAuth();
						}
					});
				})
				.catch((error) => {
					Alert.error(error.message, {
						position : 'top-right',
						effect   : 'slide',
						timeout  : 'none',
						offset   : 10
					});
				});
		}
	};

	render () {
		return (
			<div className={css(login.wrapper)}>
				<h1 className={css(login.title)}>Alert Toolbox</h1>
				<div className={css(login.container)}>
					<h2 className={css(login.subtitle)}>
						{
							this.state.signin ? 'Sign In' :
							'Sign Up'}
					</h2>
					<input
						className={css(login.input)}
						type='email'
						placeholder='Email Adress'
						value={this.state.email || ''}
						onChange={this.handleChangeEmail}
					/>
					<input
						className={css(login.input)}
						type='password'
						placeholder='Password'
						value={this.state.password || ''}
						onChange={this.handleChangePassword}
					/>
					<button
						className={css(login.button)}
						onClick={

								!this.state.authProcessing ? this.handleSubmit :
								console.log('already Authentificating')
						}>
						{
							!this.state.authProcessing ? 'Continue' :
							'Authentificating...'}
					</button>
					<span onClick={this.signMethode} className={css(login.methode)}>
						{
							this.state.signin ? <div>
								<i className={css(login.question)}>Don't have any account?</i>Sign Up
							</div> :
							<div>
								<i className={css(login.question)}>Already have an account? </i>Sign In
							</div>}
					</span>
				</div>
			</div>
		);
	}
}

export default Auth;
