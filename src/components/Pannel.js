import React from 'react';
import { FiAlignCenter, FiAlignJustify, FiAlignLeft, FiAlignRight, FiSave, FiDownload, FiUpload } from 'react-icons/fi';
import { IoIosCube, IoIosApps, IoIosAlbums, IoIosJournal, IoIosCreate, IoIosTimer } from 'react-icons/io';
import { AiOutlineFontSize, AiOutlineBorderOuter, AiOutlineCloudUpload } from 'react-icons/ai';
import MultiSlider, { Progress, Dot } from 'react-multi-bar-slider';
import { readDirectory } from '../services/System';
import { pannel, anim } from '../styles/Styles';
import { files } from '../mock/Index';
import { update, fetch } from '../services/Database';
import { Animated } from 'react-animated-css';
import { StyleSheet, css } from 'aphrodite';
import COLORS from '../styles/Colors';
import Switch from 'react-switch';
import 'rodal/lib/rodal.css';
import Rodal from 'rodal';
import '../styles/Animate.css';

const remote = window.require('electron').remote;
const fs = remote.require('fs');

class Pannel extends React.Component {
	constructor (props) {
		super(props);
		this.state = {
			text         : {
				font   : 'montserrat',
				align  : 'center',
				size   : 25,
				color  : '#FFFFFF',
				shadow : false
			},
			container    : {
				background   : '#16161D',
				borderRadius : 'semi',
				borderWidth  : 50,
				borderColor  : '#FFFFFF',
				shadow       : true
			},
			animation    : {
				in      : 'fadeInDown',
				out     : 'fadeOutDown',
				timeout : 3000
			},
			elements     : [],
			observer     : [],

			rodalVisible : false,
			saved        : true
		};
		this.handleChangeTextShadow = this.handleChangeTextShadow.bind(this);
		this.handleChangeBoxShadow = this.handleChangeBoxShadow.bind(this);
		this.hideRodal = this.hideRodal.bind(this);
		this.showRodal = this.showRodal.bind(this);
		this.initialState = this.state;
	}

	//ANCHOR: UPDATE AUTO OBSERVER
	//! ADD AUTO UPDATE OBSERVER
	//!	FOR READ EVERY 5 SEC LABELS DIRECTORY
	//!	AND SYNC WITH WEB APP

	//ANCHOR: USER UNIC URL
	//! AT ACCOUNT CREATION, CREATE A AUTOMATIC URL FOR EACH USERS
	//!	EG: HTTPS://ALERT-TOOLBOX.COM/OBSERVER?ID=6SDFE85VG5Nf7vfWwKyUOl1CIT13

	//ANCHOR: GIVE USER URL
	//!	THIS URL SHOULD BE PRINTED JUST UNDER THE PREVIEW CONTAINER
	//!	IN A READ ONLY INPUT WITH A COPY TO CLIPBOARD BUTTON

	componentDidMount () {
		this.setState({ directory: this.props.dir });
		this.handleSourceFiles();
		this.updator(); //! SHOULD BE AT THE END OF MOUNT
	}

	updator () {
		setInterval(() => {
			if (this.state !== this.initialState) {
				console.log('updating...');
				this.setState({ saved: true });
				this.initialState = this.state;
				update(this.state, this.props.uid);
			}
		}, 4000);
	}

	componentDidUpdate (prevProps, prevState) {}

	handleInputCheck = async (newValue, dir) => {
		let value = ''; //! only temp
		value = await fs.readFileSync(`${dir}/${newValue}`, 'utf-8');

		let { elements, observer } = this.state;
		let inc = { label: newValue.slice(0, -4).split('_').join(' '), value: value };

		for (var i in elements) {
			if (elements[i].name === newValue) {
				elements[i].isChecked = !elements[i].isChecked;
				if (elements[i].isChecked) {
					observer = [ ...observer, inc ];
				}
				else {
					observer = observer.filter(function (el) {
						return el.label !== inc.label;
					});
				}
				break;
			}
		}
		this.setState({ elements, observer });
	};

	handleSourceFiles = async () => {
		let { dir, uid } = this.props;
		const observer = await fetch.observer(uid);
		const config = await fetch.config(uid);
		if (observer) this.setState({ observer });
		if (config) {
			this.setState({
				text      : config.text,
				container : config.container,
				animation : config.animation,
				elements  : config.elements
			});
		}
		else {
			let filesArr = [];
			if (dir) {
				try {
					filesArr = readDirectory(dir);
				} catch (error) {
					console.log(error);
				}
			}
			let elements = [];
			filesArr.forEach((file) => {
				elements = [ ...elements, { name: file, isChecked: false } ];
			});
			this.setState({ elements });
		}
	};

	showRodal = () => this.setState({ rodalVisible: true });

	hideRodal = () => this.setState({ rodalVisible: false });

	handleChangeTextShadow = (textShadow) =>
		this.setState({ text: { ...this.state.text, shadow: textShadow }, saved: false });

	handleChangeBoxShadow = (boxShadow) =>
		this.setState({ container: { ...this.state.container, shadow: boxShadow }, saved: false });

	handleTextSlide = (newSize) =>
		this.setState({ container: { ...this.state.container, borderWidth: newSize }, saved: false });

	handleBorderSlide = (newWidth) => {
		if (newWidth >= 0) this.setState({ container: { ...this.state.container, borderWidth: 0 }, saved: false });
		if (newWidth >= 25) this.setState({ container: { ...this.state.container, borderWidth: 25 }, saved: false });
		if (newWidth >= 50) this.setState({ container: { ...this.state.container, borderWidth: 50 }, saved: false });
		if (newWidth >= 75) this.setState({ container: { ...this.state.container, borderWidth: 75 }, saved: false });
		if (newWidth >= 100) this.setState({ container: { ...this.state.container, borderWidth: 100 }, saved: false });
	};

	handleChangeDuration = (event) =>
		this.setState({ animation: { ...this.state.animation, timeout: event.target.value }, saved: false });

	handleInputAnimation = (event) => {
		this.setState({ animation: { ...this.state.animation, in: event.target.value }, saved: false });
	};

	handleOutputAnimation = (event) => {
		this.setState({ animation: { ...this.state.animation, out: event.target.value }, saved: false });
	};

	handleChangeTextSize = (event) => {
		let size = event.target.value;
		size = size.replace(' px', '');
		if (!isNaN(size) && size !== '')
			this.setState({ text: { ...this.state.text, size: parseInt(size) }, saved: false });
	};

	handleChangeBorderWidth = (event) => {
		let width = event.target.value;
		width = width.replace(' px', '');
		if (!isNaN(width) && width !== '')
			this.setState({ container: { ...this.state.container, borderWidth: parseInt(width) }, saved: false });
	};

	handleChangeBackground = (event) => {
		if (event.target.value !== '')
			this.setState({ container: { ...this.state.container, background: event.target.value }, saved: false });
		else this.setState({ container: { ...this.state.container, background: event.target.value }, saved: false });
	};

	handleChangeFont = (event) => {
		this.setState({ text: { ...this.state.text, font: event.target.value }, saved: false });
		console.log('state:', this.state);
	};

	handleChangeTextColor = (event) => {
		if (event.target.value !== '')
			this.setState({ text: { ...this.state.text, color: event.target.value }, saved: false });
		else this.setState({ text: { ...this.state.text, color: '#FFFFFF' }, saved: false });
	};

	handleChangeBorderColor = (event) => {
		if (event.target.value !== '')
			this.setState({ container: { ...this.state.container, borderColor: event.target.value }, saved: false });
		else this.setState({ container: { ...this.state.container, borderColor: '#FFFFFF' }, saved: false });
	};

	setAlignActive = (align) => this.setState({ text: { ...this.state.text, align: align }, saved: false });

	setRadiusActive = (radius) =>
		this.setState({ container: { ...this.state.container, borderRadius: radius }, saved: false });

	signMethode = () => this.setState({ signin: !this.state.signin, saved: false });

	render () {
		return (
			<div className={css(pannel.wrapper)}>
				<PannelName name={this.props.active} />
				{
					this.props.active === 'text' ? <React.Fragment>
						<PannelTextAlign selected={this.state.text.align} setAlignActive={this.setAlignActive} />
						<PannelTextFont
							font={this.state.text.font}
							size={this.state.text.size}
							color={this.state.text.color}
							handleSlide={this.handleTextSlide}
							handleChangeFont={this.handleChangeFont}
							handleChangeSize={this.handleChangeTextSize}
							handleChangeColor={this.handleChangeTextColor}
						/>
						<PannelTextShadow handleChangeShadow={this.handleChangeTextShadow} shadow={this.state.text.shadow} />
					</React.Fragment> :
					null}
				{
					this.props.active === 'elements' ? <PannelElements
						show={this.showRodal}
						hide={this.hideRodal}
						visible={this.state.rodalVisible}
						handleInputCheck={this.handleInputCheck}
						files={this.state.elements}
						directory={this.props.dir}
					/> :
					null}
				{
					this.props.active === 'animations' ? <React.Fragment>
						<PannelAnimations
							handleInputAnimation={this.handleInputAnimation}
							handleOutputAnimation={this.handleOutputAnimation}
							inputAnimation={this.state.animation.in}
							outputAnimation={this.state.animation.out}
						/>
						<PannelDuration handleChangeDuration={this.handleChangeDuration} duration={this.state.animation.timeout} />
						<AnimationExample input={this.state.animation.in} output={this.state.animation.out} />
					</React.Fragment> :
					null}
				{
					this.props.active === 'containers' ? <React.Fragment>
						<PannelContainers
							background={this.state.container.background}
							handleChangeBackground={this.handleChangeBackground}
						/>
						<PannelContainerBorder
							size={this.state.container.borderWidth}
							selected={this.state.container.borderRadius}
							setRadiusActive={this.setRadiusActive}
							color={this.state.container.borderColor}
							handleChangeColor={this.handleChangeBorderColor}
							handleSlide={this.handleBorderSlide}
							handleChangeSize={this.handleChangeBorderWidth}
						/>
						<PannelContainerShadow
							handleChangeShadow={this.handleChangeBoxShadow}
							shadow={this.state.container.shadow}
						/>
					</React.Fragment> :
					null}
				{
					this.props.active === 'files' ? <PannelConfig /> :
					null}
				<SaveConfig saved={this.state.saved} />
			</div>
		);
	}
}

const SaveConfig = (props) => {
	return (
		<Animated
			animationIn='fadeInRight'
			animationOut='fadeOutRight'
			isVisible={!props.saved}
			className={css(pannel.floatingNotif)}>
			<button className={css(pannel.floatButton)} onClick={props.hide}>
				<AiOutlineCloudUpload className={css(pannel.inputFileIcon)} />Updating...
			</button>
		</Animated>
	);
};

const PannelAnimations = (props) => {
	return (
		<div className={css(pannel.section)}>
			<h4 className={css(pannel.titleSection)}>Input Animation</h4>
			<select value={props.inputAnimation} onChange={props.handleInputAnimation} className={css(pannel.inputSelect)}>
				<optgroup label='Bouncing Entrances'>
					<option value='bounceIn'>bounceIn</option>
					<option value='bounceInDown'>bounceInDown</option>
					<option value='bounceInLeft'>bounceInLeft</option>
					<option value='bounceInRight'>bounceInRight</option>
					<option value='bounceInUp'>bounceInUp</option>
				</optgroup>
				<optgroup label='Fading Entrances'>
					<option value='fadeIn'>fadeIn</option>
					<option value='fadeInDown'>fadeInDown</option>
					<option value='faldeInLeft'>faldeInLeft</option>
					<option value='fadeInRight'>fadeInRight</option>
					<option value='fadeInUp'>fadeInUp</option>
				</optgroup>
				<optgroup label='Flippers'>
					<option value='flipInX'>flipInX</option>
					<option value='flipInY'>flipInY</option>
				</optgroup>
				<optgroup label='LightSpeed'>
					<option value='lightSpeedIn'>lightSpeedIn</option>
				</optgroup>
				<optgroup label='Ratating Entrances'>
					<option value='rotateIn'>rotateIn</option>
					<option value='rotateInDownLeft'>rotateInDownLeft</option>
					<option value='rotateInDownRight'>rotateInDownRight</option>
					<option value='rotateInUpLeft'>rotateInUpLeft</option>
					<option value='rotateInUpRight'>rotateInUpRight</option>
				</optgroup>
				<optgroup label='Sliding Entrances'>
					<option value='slideInDown'>slideInDown</option>
					<option value='slideInLeft'>slideInLeft</option>
					<option value='slideInRight'>slideInRight</option>
					<option value='slideInUp'>slideInUp</option>
				</optgroup>
				<optgroup label='Zooming Entrances'>
					<option value='zoomIn'>zoomIn</option>
					<option value='zoomInUp'>zoomInUp</option>
					<option value='zoomInLeft'>zoomInLeft</option>
					<option value='zoomInRight'>zoomInRight</option>
					<option value='zoomInUp'>zoomInUp</option>
				</optgroup>
			</select>
			<h4 className={css(pannel.titleSection)}>Output Animation</h4>
			<select value={props.outputAnimation} onChange={props.handleOutputAnimation} className={css(pannel.inputSelect)}>
				<optgroup label='Bouncing Exits'>
					<option value='bounceOut'>bounceOut</option>
					<option value='bounceOutDown'>bounceOutDown</option>
					<option value='bounceOutLeft'>bounceOutLeft</option>
					<option value='bounceOutRight'>bounceOutRight</option>
					<option value='bounceOutUp'>bounceOutUp</option>
				</optgroup>
				<optgroup label='Fading Exits'>
					<option value='fadeOut'>fadeOut</option>
					<option value='fadeOutDown'>fadeOutDown</option>
					<option value='faldeOutLeft'>faldeOutLeft</option>
					<option value='fadeOutRight'>fadeOutRight</option>
					<option value='fadeOutUp'>fadeOutUp</option>
				</optgroup>
				<optgroup label='Flippers'>
					<option value='flipOutX'>flipOutX</option>
					<option value='flipOutY'>flipOutY</option>
				</optgroup>
				<optgroup label='LightSpeed'>
					<option value='lightSpeedOut'>lightSpeedOut</option>
				</optgroup>
				<optgroup label='Ratating Exits'>
					<option value='rotateOut'>rotateOut</option>
					<option value='rotateOutDownLeft'>rotateOutDownLeft</option>
					<option value='rotateOutDownRight'>rotateOutDownRight</option>
					<option value='rotateOutUpLeft'>rotateOutUpLeft</option>
					<option value='rotateOutUpRight'>rotateOutUpRight</option>
				</optgroup>
				<optgroup label='Sliding Exits'>
					<option value='slideOutDown'>slideOutDown</option>
					<option value='slideOutLeft'>slideOutLeft</option>
					<option value='slideOutRight'>slideOutRight</option>
					<option value='slideOutUp'>slideOoutUp</option>
				</optgroup>
				<optgroup label='Zooming Exits'>
					<option value='zoomOut'>zoomOut</option>
					<option value='zoomOutUp'>zoomOutUp</option>
					<option value='zoomOutLeft'>zoomOutLeft</option>
					<option value='zoomOutRight'>zoomOutRight</option>
					<option value='zoomOutUp'>zoomOutUp</option>
				</optgroup>
			</select>
		</div>
	);
};

const AnimationExample = (props) => {
	return (
		<div className={css(pannel.section)}>
			<div className={css(pannel.titlesRow)}>
				<h4 className={css(pannel.titleSection)}>Input</h4>
				<h4 className={css(pannel.titleSection)}>Output</h4>
			</div>
			<div className={css(pannel.titlesRow)}>
				<div className={css(pannel.exampleBox, anim.config)}>INPUT</div>
				<div className={css(pannel.exampleBox, anim.config)}>OUTPUT</div>
			</div>
		</div>
	);
};

const PannelDuration = (props) => {
	return (
		<div className={css(pannel.section)}>
			<h4 className={css(pannel.titleSection)}>Delay before Output Animation</h4>
			<div className={css(pannel.durationSelector)}>
				<IoIosTimer className={css(pannel.durationIcon)} />
				<input
					type='number'
					value={props.duration}
					className={css(pannel.inputNumValue)}
					onChange={props.handleChangeDuration}
				/>
				<h4 className={css(pannel.titleSection, pannel.centeredInputText)}>Miliseconds</h4>
			</div>
		</div>
	);
};

const PannelName = (props) => {
	let pannelName = null;
	switch (props.name) {
		case 'animations':
			pannelName = (
				<div className={css(pannel.pannelName)}>
					<IoIosAlbums className={css(pannel.pannelIcon)} />
					<span>Animations</span>
				</div>
			);
			break;
		case 'containers':
			pannelName = (
				<div className={css(pannel.pannelName)}>
					<IoIosCube className={css(pannel.pannelIcon)} />
					<span style={{ padding: '20px 0' }}>Containers</span>
				</div>
			);
			break;

		case 'elements':
			pannelName = (
				<div className={css(pannel.pannelName)}>
					<IoIosApps className={css(pannel.pannelIcon)} />
					<span>Elements</span>
				</div>
			);
			break;
		case 'text':
			pannelName = (
				<div className={css(pannel.pannelName)}>
					<IoIosCreate className={css(pannel.pannelIcon)} />
					<span>Text Settings</span>
				</div>
			);
			break;
		case 'files':
			pannelName = (
				<div className={css(pannel.pannelName)}>
					<IoIosJournal className={css(pannel.pannelIcon)} />
					<span style={{ padding: '20px 0' }}>My Files</span>
				</div>
			);
			break;
		default:
			pannelName = (
				<div className={css(pannel.pannelName)}>
					<IoIosAlbums className={css(pannel.pannelIcon)} />
					<span>Templates</span>
				</div>
			);
			break;
	}
	return pannelName;
};

const PannelTextShadow = (props) => {
	return (
		<div className={css(pannel.section, pannel.sectionRow)}>
			<h4 className={css(pannel.titleSection)} style={{ lineHeight: '3em', marginTop: '0' }}>
				Shadow
			</h4>
			<Switch
				height={18}
				width={40}
				onColor={COLORS.INFO}
				offColor={COLORS.PLACEHOLDER}
				onChange={props.handleChangeShadow}
				checked={props.shadow || false}
				checkedIcon={false}
				uncheckedIcon={false}
			/>
		</div>
	);
};

const PannelTextAlign = (props) => {
	return (
		<div className={css(pannel.section)}>
			<h4 className={css(pannel.titleSection)}>Alignment</h4>
			<ul className={css(pannel.alignList)}>
				<li
					onClick={() => props.setAlignActive('left')}
					className={css(
						pannel.alignItem,
						pannel.alignItemLeft,

							props.selected === 'left' ? pannel.alignActive :
							null
					)}>
					<FiAlignLeft />
				</li>
				<li
					onClick={() => props.setAlignActive('center')}
					className={css(
						pannel.alignItem,

							props.selected === 'center' ? pannel.alignActive :
							null
					)}>
					<FiAlignCenter />
				</li>
				<li
					onClick={() => props.setAlignActive('right')}
					className={css(
						pannel.alignItem,

							props.selected === 'right' ? pannel.alignActive :
							null
					)}>
					<FiAlignRight />
				</li>
				<li
					onClick={() => props.setAlignActive('justify')}
					className={css(
						pannel.alignItem,
						pannel.alignItemRight,

							props.selected === 'justify' ? pannel.alignActive :
							null
					)}>
					<FiAlignJustify />
				</li>
			</ul>
		</div>
	);
};

const PannelTextFont = (props) => {
	return (
		<div className={css(pannel.section)}>
			<h4 className={css(pannel.titleSection)}>Font</h4>
			<select value={props.font} onChange={props.handleChangeFont} className={css(pannel.inputSelect)}>
				<option value='comfortaa'>Comfortaa</option>
				<option value='montserrat'>Montserrat</option>
				<option value='newRomans'>New Romans</option>
				<option value='openSans'>Open Sans</option>
				<option value='roboto'>Roboto</option>
			</select>
			<h4 className={css(pannel.titleSection)}>Size</h4>
			<div className={css(pannel.fontSizer)}>
				<AiOutlineFontSize className={css(pannel.fontSizeIcon)} />
				<MultiSlider
					width={120}
					height={4}
					slidableZoneSize={20}
					backgroundColor={COLORS.PLACEHOLDER}
					equalColor={COLORS.INFO}
					onSlide={props.handleSlide}
					roundedCorners>
					<Progress color={COLORS.INFO} height={4} progress={props.size}>
						<Dot color={COLORS.CONTRAST} width={12} height={12} />
					</Progress>
				</MultiSlider>
				<input value={`${props.size} px`} className={css(pannel.inputSimpleValue)} onChange={props.handleChangeSize} />
			</div>
			<h4 className={css(pannel.titleSection)}>Color</h4>
			<div className={css(pannel.colorSelector)}>
				<div className={css(pannel.colorOutline)}>
					<div className={css(pannel.colorDot)} style={{ backgroundColor: props.color }} />
				</div>
				<input value={props.color} className={css(pannel.inputHex)} onChange={props.handleChangeColor} />
			</div>
		</div>
	);
};

const PannelElements = (props) => {
	console.log(props.directory);
	return (
		<React.Fragment>
			<div className={css(pannel.section)}>
				<PannelElementsTag
					files={props.files}
					visible={props.visible}
					hide={props.hide}
					directory={props.directory}
					handleInputCheck={props.handleInputCheck}
				/>
				<button className={css(pannel.inputFile)} onClick={props.show}>
					<IoIosApps className={css(pannel.inputFileIcon)} />Select Elements
				</button>
			</div>
		</React.Fragment>
	);
};

const PannelElementsTag = (props) => {
	const list = props.files.map((item, index) => (
		<div key={index} className={css(pannel.modalGridItem)}>
			<input
				className={css(pannel.modalCheckBoxLeft)}
				type='checkbox'
				checked={item.isChecked}
				onChange={() => props.handleInputCheck(item.name, props.directory)}
			/>
			{item.name.slice(0, -4).split('_').join(' ')}
		</div>
	));
	return (
		<Rodal
			visible={props.visible}
			onClose={props.hide}
			width={600}
			height={400}
			customStyles={{ backgroundColor: COLORS.CARD, border: '1px solid rgba(255,255,255, 0.1)', borderRadius: '8px' }}
			customMaskStyles={{ opacity: 0 }}>
			<div className={css(pannel.modalGrid)} style={{ height: '100%', overflow: 'auto' }}>
				<h4 className={css(pannel.modalTitle)}>Elements you want to display</h4>
				{list}
				{
					files.length <= 0 ? <em className={css(pannel.modalError)}>
						Oops...<br />Something goes wrong<br />while listing files!
					</em> :
					null}
			</div>
		</Rodal>
	);
};

const PannelContainerShadow = (props) => {
	return (
		<div className={css(pannel.section, pannel.sectionRow)}>
			<h4 className={css(pannel.titleSection)} style={{ lineHeight: '3em', marginTop: '0' }}>
				Shadow
			</h4>
			<Switch
				height={18}
				width={40}
				onColor={COLORS.INFO}
				offColor={COLORS.PLACEHOLDER}
				onChange={props.handleChangeShadow}
				checked={props.shadow || false}
				checkedIcon={false}
				uncheckedIcon={false}
			/>
		</div>
	);
};

const PannelContainerBorder = (props) => {
	let inputSize = 2;
	if (props.size >= 0) inputSize = 0;
	if (props.size >= 25) inputSize = 1;
	if (props.size >= 50) inputSize = 2;
	if (props.size >= 75) inputSize = 3;
	if (props.size >= 100) inputSize = 4;
	return (
		<div className={css(pannel.section)}>
			<h4 className={css(pannel.titleSection)}>Radius</h4>
			<ul className={css(pannel.alignList)} style={{ width: '100%' }}>
				<li
					onClick={() => props.setRadiusActive('none')}
					className={css(
						pannel.alignItem,
						pannel.alignItemText,
						pannel.alignItemLeft,

							props.selected === 'none' ? pannel.alignActive :
							null
					)}>
					None
				</li>
				<li
					onClick={() => props.setRadiusActive('light')}
					className={css(
						pannel.alignItem,
						pannel.alignItemText,

							props.selected === 'light' ? pannel.alignActive :
							null
					)}>
					Light
				</li>
				<li
					onClick={() => props.setRadiusActive('semi')}
					className={css(
						pannel.alignItem,
						pannel.alignItemText,

							props.selected === 'semi' ? pannel.alignActive :
							null
					)}>
					Semi
				</li>
				<li
					onClick={() => props.setRadiusActive('full')}
					className={css(
						pannel.alignItem,
						pannel.alignItemText,
						pannel.alignItemRight,

							props.selected === 'full' ? pannel.alignActive :
							null
					)}>
					Full
				</li>
			</ul>
			<h4 className={css(pannel.titleSection)}>Width </h4>
			<div className={css(pannel.fontSizer)}>
				<AiOutlineBorderOuter className={css(pannel.fontSizeIcon)} />
				<MultiSlider
					width={120}
					height={4}
					slidableZoneSize={1}
					backgroundColor={COLORS.PLACEHOLDER}
					equalColor={COLORS.INFO}
					onSlide={props.handleSlide}
					roundedCorners>
					<Progress color={COLORS.INFO} height={4} progress={props.size}>
						<Dot color={COLORS.CONTRAST} width={12} height={12} />
					</Progress>
				</MultiSlider>
				<input value={`${inputSize} px`} className={css(pannel.inputSimpleValue)} onChange={props.handleChangeSize} />
			</div>
			<h4 className={css(pannel.titleSection)}>Color</h4>
			<div className={css(pannel.colorSelector)}>
				<div className={css(pannel.colorOutline)}>
					<div className={css(pannel.colorDot)} style={{ backgroundColor: props.color }} />
				</div>
				<input value={props.color} className={css(pannel.inputHex)} onChange={props.handleChangeColor} />
			</div>
		</div>
	);
};

const PannelContainers = (props) => {
	return (
		<div className={css(pannel.section)}>
			<h4 className={css(pannel.titleSection)}>Label Container </h4>

			<select value={props.value} onChange={props.handleChangeFont} className={css(pannel.inputSelect)}>
				<option value='comfortaa'>Rounded</option>
				<option value='openSans'>Semi</option>
				<option value='roboto'>None</option>
			</select>
			<h4 className={css(pannel.titleSection)}>Background</h4>
			<div className={css(pannel.colorSelector)}>
				<div className={css(pannel.colorOutline)}>
					<div className={css(pannel.colorDot)} style={{ backgroundColor: props.background }} />
				</div>
				<input value={props.background} className={css(pannel.inputHex)} onChange={props.handleChangeBackground} />
			</div>
		</div>
	);
};

const PannelConfig = (props) => {
	const button = StyleSheet.create({
		save : {
			background  : COLORS.PRIMARY,
			color       : COLORS.CARD,
			borderColor : COLORS.PRIMARY,
			transition  : '500ms',
			':hover'    : {
				filter    : 'brightness(1.1)',
				boxShadow : `0px 2px 40px -10px ${COLORS.PRIMARY}`
			}
		}
	});
	return (
		<React.Fragment>
			<div className={css(pannel.section)}>
				<h4 className={css(pannel.titleSection)} style={{ paddingBottom: '15px' }}>
					Configuration files
				</h4>
				<button className={css(pannel.inputFile, pannel.inputConfig)} onClick={props.show} style={{ width: '100%' }}>
					<FiDownload className={css(pannel.inputFileIcon)} />Import Json
				</button>
				<button className={css(pannel.inputFile, pannel.inputConfig)} onClick={props.show} style={{ width: '100%' }}>
					<FiUpload className={css(pannel.inputFileIcon)} />Export Json
				</button>
				<button
					className={css(pannel.inputFile, pannel.inputConfig, button.save)}
					onClick={props.show}
					style={{ width: '100%' }}>
					<FiSave className={css(pannel.inputFileIcon)} />Save or Reload config
				</button>
			</div>
			<div className={css(pannel.section)}>
				<h4 className={css(pannel.titleSection)} style={{ paddingBottom: '15px' }}>
					Raw
				</h4>
				<p className={css(pannel.rawInput)}>{JSON.stringify(files)}</p>
			</div>
		</React.Fragment>
	);
};
export default Pannel;
