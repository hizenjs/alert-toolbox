import React from 'react';
import { css } from 'aphrodite';
import { sidenav } from '../styles/Styles';
import { IoIosApps, IoIosAlbums, IoIosJournal, IoIosCreate, IoIosCube } from 'react-icons/io';
import '../styles/Loader.css';

class SideNav extends React.Component {
	constructor (props) {
		super(props);
		this.state = {
			signin : true,
			active : 'text'
		};
	}

	signMethode = () => {
		this.setState({ signin: !this.state.signin });
	};

	render () {
		return (
			<div className={css(sidenav.wrapper)}>
				<div
					onClick={() => this.props.select('animations')}
					className={css(
						sidenav.item,

							this.props.active === 'animations' ? sidenav.active :
							null
					)}>
					<IoIosAlbums size={24} />
					Animations
				</div>
				<div
					onClick={() => this.props.select('containers')}
					className={css(
						sidenav.item,

							this.props.active === 'containers' ? sidenav.active :
							null
					)}>
					<IoIosCube size={24} />
					Containers
				</div>
				<div
					onClick={() => this.props.select('elements')}
					className={css(
						sidenav.item,

							this.props.active === 'elements' ? sidenav.active :
							null
					)}>
					<IoIosApps size={24} />
					Elements
				</div>
				<div
					onClick={() => this.props.select('text')}
					className={css(
						sidenav.item,

							this.props.active === 'text' ? sidenav.active :
							null
					)}>
					<IoIosCreate size={24} />
					Text
				</div>
				<div
					onClick={() => this.props.select('files')}
					className={css(
						sidenav.item,

							this.props.active === 'files' ? sidenav.active :
							null
					)}>
					<IoIosJournal size={24} />
					My Files
				</div>
			</div>
		);
	}
}

export default SideNav;
