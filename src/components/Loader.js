import React from 'react'
import { css } from 'aphrodite';
import { loader } from '../styles/Styles'
import '../styles/Loader.css'

const AppLoader = (props) => {
  return (
    <div className={css(loader.wrapper)}>
      <Loader />
    </div>
  )

}


const Loader = (props) => {
  return (
    <div className="cube-container">
      <div className="cube">
        <div className="front"></div>
        <div className="back"></div>
        <div className="right"></div>
        <div className="left"></div>
        <div className="top"></div>
        <div className="bottom"></div>
      </div>
    </div>
  )
}

export {AppLoader, Loader}