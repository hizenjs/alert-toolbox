import React from 'react';
import { css } from 'aphrodite';
import { dropdown } from '../styles/Styles';
import { IoIosArrowDown, IoIosArrowUp, IoIosSave, IoIosLock, IoIosSettings } from 'react-icons/io';
import Alert from 'react-s-alert';

export default class Dropdown extends React.Component {
	constructor (props) {
		super(props);
		this.state = {
			showMenu : false
		};
		this.showMenu = this.showMenu.bind(this);
		this.closeMenu = this.closeMenu.bind(this);
		this.toast = this.toast.bind(this);
	}

	toast () {
		Alert.warning("We're actually in dev stack", {
			position : 'top-right',
			effect   : 'slide',
			beep     : true,
			timeout  : 3000,
			offset   : 0
		});
	}

	showMenu (event) {
		event.preventDefault();

		this.setState({ showMenu: true }, () => {
			document.addEventListener('click', this.closeMenu);
		});
	}

	closeMenu (event) {
		if (!this.dropdownMenu.contains(event.target)) {
			this.setState({ showMenu: false }, () => {
				document.removeEventListener('click', this.closeMenu);
			});
		}
	}

	signOut () {
		this.closeMenu();
		this.props.logout();
	}

	render () {
		return (
			<div>
				{
					this.state.showMenu ? <IoIosArrowUp
						onClick={this.showMenu}
						className={css(dropdown.iconDropdown)}
						color={'white'}
					/> :
					<IoIosArrowDown onClick={this.showMenu} className={css(dropdown.iconDropdown)} color={'white'} />}

				{
					this.state.showMenu ? <div
						className={css(dropdown.menu)}
						ref={(element) => {
							this.dropdownMenu = element;
						}}>
						<p className={css(dropdown.item)}>
							<IoIosSettings className={css(dropdown.icon)} />Settings
						</p>
						<p className={css(dropdown.item)} onClick={this.toast}>
							<IoIosSave className={css(dropdown.icon)} />Recovery
						</p>
						<p className={css(dropdown.item)} onClick={this.signOut}>
							<IoIosLock className={css(dropdown.icon)} />Disconnect
						</p>
					</div> :
					null}
			</div>
		);
	}
}
