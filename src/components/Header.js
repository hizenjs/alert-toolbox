import React from 'react';
import { css } from 'aphrodite';
import { header } from '../styles/Styles';
import { FiFileText, FiImage } from 'react-icons/fi';
import Dropdown from './Dropdown';
import logo from '../assets/logo.png';
import profile from '../assets/profile.png';
import { Titlebar, Color } from 'custom-electron-titlebar';
import COLORS from '../styles/Colors';
import { database } from '../services/Config';
import Alert from 'react-s-alert';

const remote = window.require('electron').remote;
const { dialog } = remote;

export const titlebar = new Titlebar({
	backgroundColor: Color.fromHex(COLORS.CARD),
	closeable: true,
	minimizable: true
});

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: '600x254'
		};
		this.handleChange = this.handleChange.bind(this);
	}

	componentDidMount() {
		titlebar.updateBackground(Color.fromHex(COLORS.CARD));
	}

	handleChange = (event) => {
		this.props.handleRes(event.target.value);
		this.setState({ value: event.target.value });
	};

	syncDirectory = () => {
		const { uid } = this.props;
		dialog
			.showOpenDialog({ properties: [ 'openDirectory' ], title: 'Hello World' })
			.then((res) => {
				if (res.filePaths && res.filePaths[0]) {
					database.child(uid).update({ directory: res.filePaths[0] || null }, (error) => {
						if (error) {
							Alert.error(error, {
								position: 'top-right',
								effect: 'slide',
								timeout: 3000,
								offset: 0
							});
						} else {
							Alert.success('Local directory updated!', {
								position: 'top-right',
								effect: 'slide',
								timeout: 3000,
								offset: 0
							});
						}
					});
				}
				if (res.canceled) return false;
			})
			.catch((err) => {
				alert(err);
			});
	};

	render() {
		return (
			<div className={css(header.wrapper)}>
				<div className={css(header.brand)}>
					<img className={css(header.brandImage)} src={logo} alt="logo" />
					<h1 className={css(header.brandName)}>Alert Toolbox</h1>
				</div>
				<div className={css(header.centered)}>
					<div className={css(header.format)}>
						<h4 className={css(header.formatLabel)}>Popular Formats</h4>
						<select value={this.state.value} onChange={this.handleChange} className={css(header.inputFile)}>
							<option value="1920x1080">1920x1080</option>
							<option value="1024x512">1024x512</option>
							<option value="800x512">800x512</option>
							<option value="800x254">800x254</option>
							<option value="600x254">600x254</option>
						</select>
					</div>
					<button className={css(header.inputFile)}>
						<FiImage className={css(header.inputFileIcon)} />Background
					</button>
				</div>
				<div className={css(header.account)}>
					<button className={css(header.button)} onClick={() => this.syncDirectory()}>
						<FiFileText className={css(header.inputFileIcon)} />Local
					</button>
					<div className={css(header.accountDropdown)}>
						<img className={css(header.accountImage)} src={profile} alt="logo" />
						<Dropdown logout={this.props.logout} />
					</div>
				</div>
			</div>
		);
	}
}

export default Header;
