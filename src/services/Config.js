import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const firebaseConfig = {
	apiKey: 'AIzaSyCgN6ReEGkB1YkdaA5cuxuzXTd0OgXgugA',
	authDomain: 'alert-toolbox.firebaseapp.com',
	databaseURL: 'https://alert-toolbox.firebaseio.com',
	projectId: 'alert-toolbox',
	storageBucket: 'alert-toolbox.appspot.com',
	messagingSenderId: '524705464438',
	appId: '1:524705464438:web:2a7aea69bec2578aae5f57',
	measurementId: 'G-YXPFWLTYSF'
};
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const database = firebase.database().ref('users');
