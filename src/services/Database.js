import { database } from '../services/Config';
import Alert from 'react-s-alert';

class Fetch {
	observer = async (uid) => {
		const snapshot = await database.child(uid + '/observer/').once('value');
		return snapshot.val();
	};
	config = async (uid) => {
		const snapshot = await database.child(uid + '/config/').once('value');
		return snapshot.val();
	};
	directory = async (uid) => {
		const snapshot = await database.child(uid).once('value');
		return snapshot.val().directory;
		//return 'hello';
	};
	animation = async (uid) => {
		const snapshot = await database.child(uid + '/config/').once('value');
		return snapshot.val();
	};
}

export function update (props, uid) {
	var properties = {
		text      : props.text,
		container : props.container,
		animation : props.animation,
		elements  : props.elements
	};
	var observer = props.observer;

	var updates = {};
	updates[uid + '/config/'] = properties;
	updates[uid + '/observer/'] = observer;

	return database.update(updates, function (error) {
		if (error) {
			Alert.error(error.message, {
				position : 'top-right',
				effect   : 'slide',
				timeout  : 4000,
				offset   : 0
			});
		}
		else {
			// Alert.success('Datas has been successfully saved', {
			// 	position: 'top-right',
			// 	effect: 'slide',
			// 	timeout: 2000,
			// 	offset: 0
			// });
		}
	});
}

export const fetch = new Fetch();
