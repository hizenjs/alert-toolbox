const remote = window.require('electron').remote;
const electronFs = remote.require('fs');
const { dialog } = remote;

export function readDirectory(path) {
	var fileArray = [];
	electronFs.readdirSync(path).forEach((file) => {
		fileArray = [ ...fileArray, file ];
	});
	return fileArray;
}

export function getDirectory() {
	dialog
		.showOpenDialog({ properties: [ 'openDirectory' ], title: 'Hello World' })
		.then((res) => {
			if (res.filePaths && res.filePaths[0]) return res.filePaths[0];
			if (res.canceled) return false;
		})
		.catch((err) => {
			alert(err);
		});
}
