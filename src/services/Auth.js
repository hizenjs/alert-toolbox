import { auth, database } from './Config';
import Alert from 'react-s-alert';

export const signup = (email, password) => {
	auth
		.createUserWithEmailAndPassword(email, password)
		.then((res) => {
			database.ref('users').child(res.user.uid).set({
				email : res.user.uid
			}, (error) => {
				if (error) {
					Alert.error(error, {
						position : 'top-right',
						effect   : 'slide',
						timeout  : 'none',
						offset   : 10
					});
				}
				else {
					return 'hello';
				}
			});
		})
		.catch((error) => {
			Alert.error(error.message, {
				position : 'top-right',
				effect   : 'slide',
				timeout  : 'none',
				offset   : 10
			});
		});
};

export const signin = (email, password) => {
	auth.signInWithEmailAndPassword(email, password).then((res) => {
		if (res.user.uid) return res.user.uid;
	});
};
