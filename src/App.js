import React from 'react';
import { AppLoader } from './components/Loader';
import { database } from './services/Config';
import Auth from './components/Auth';
import Header from './components/Header';
import SideNav from './components/SideNav';
import Pannel from './components/Pannel';
import Preview from './components/Preview';
import * as firebase from 'firebase/app';
import calculateAspectRatio from 'calculate-aspect-ratio';

class App extends React.Component {
	constructor (props) {
		super(props);
		this.state = {
			stateReady  : false,
			isLoggedIn  : false,
			resolution  : '600x254',
			aspectRatio : '16:9',
			pannel      : 'animations'
		};
	}

	async componentWillMount () {
		const screenScale = this.state.resolution.split('x');
		this.setState({ aspectRatio: calculateAspectRatio(screenScale[0], screenScale[1]) });
		const self = this;
		await firebase.auth().onAuthStateChanged(function (user) {
			if (user) {
				setTimeout(() => {
					return database.child(user.uid).once('value').then((snap) => {
						self.setState({
							db         : snap.val(),
							stateReady : true,
							isLoggedIn : true,
							uid        : user.uid
						});
					});
				}, 0);
			}
			else {
				setTimeout(() => {
					self.setState({ stateReady: !self.state.stateReady });
				}, 0);
			}
		});
	}

	authServices = () => {
		this.setState({ isLoggedIn: true });
	};

	logout = () => {
		const self = this;
		firebase.auth().signOut().then(() => self.setState({ isLoggedIn: false }));
	};

	resolution = (resolution) => {
		const screenScale = resolution.split('x');
		this.setState({ resolution: resolution, aspectRatio: calculateAspectRatio(screenScale[0], screenScale[1]) });
		console.log(this.state.resolution + ' = ' + this.state.aspectRatio);
	};

	select = (pannel) => {
		this.setState({ pannel });
	};

	render () {
		if (this.state.stateReady) {
			if (this.state.isLoggedIn) {
				return (
					<React.Fragment>
						<Header handleRes={this.resolution} logout={this.logout} uid={this.state.uid} />
						<SideNav active={this.state.pannel} select={this.select} />
						<Preview ratio={this.state.aspectRatio} reso={this.state.resolution} />
						<Pannel
							active={this.state.pannel}
							uid={this.state.uid}
							dir={

									this.state.db ? this.state.db.directory :
									null
							}
							config={

									this.state.db ? this.state.db.config :
									null
							}
						/>
					</React.Fragment>
				);
			}
			else return <Auth handleAuth={() => this.authServices()} />;
		}
		else {
			return <AppLoader />;
		}
	}
}

export default App;
